.. _installation-configuration-auth:

Authentication
==============

This section explains all configuration related to user authentication.

.. confval:: AUTH_PROVIDERS

    This configuration value specifies the authentication providers to be used for
    logging in to |kadi|. One or more providers can be specified as a list of
    dictionaries, where each dictionary needs to at least contain the type of the
    authentication provider. Please see the sections below for the provider-specific
    configuration options and other details.

    Defaults to:

    .. code-block:: python3

        [
            {
                "type": "local",
            },
        ]

.. tip::
    :ref:`Sysadmins <installation-configuration-sysadmins>` can also manage various
    aspects regarding existing users via the graphical sysadmin interface, e.g.
    activating or deactivating individual user accounts. Certain aspects may also be
    managed via the ``kadi users`` command of the Kadi CLI, which can be especially
    helpful for the initial user setup after completing the configuration:

    .. code-block:: bash

        sudo su - kadi    # Switch to the kadi user (in production environments)
        kadi users --help # Get a list of user management commands

Provider configuration
----------------------

Each authentication provider supports various configuration options that may need to be
changed in order to make the provider work properly. The following sections describe
these options as well as additional details about each provider, such as how the most
important user attributes are handled.

Local
~~~~~

Accounts based on local authentication are managed by |kadi| itself, i.e. separate user
accounts are required, but this option is also the easiest one to set up. The most
important user attributes are handled the following:

* **Username:** Set by the user (or sysadmin), but cannot be changed afterwards.
* **Email:** Set by the user (or sysadmin). Can be changed by the user, but is not
  confirmed automatically.
* **Password:** Set by the user (or generated automatically). Can be changed by the
  user.

The following configuration options are supported by this provider:

.. confval:: type
    :noindex:

    The type of the authentication provider. Must correspond to ``"local"``.

.. confval:: title
    :noindex:

    The title shown on the login page corresponding to the authentication provider.

    Defaults to ``"Login with credentials"`` (localized).

.. confval:: default_system_role
    :noindex:

    The default system role of newly registered accounts. One of ``"admin"``,
    ``"member"`` or ``"guest"``. Please see the user management section in the graphical
    sysadmin interface for more information about system roles.

    Defaults to ``"member"``.

.. confval:: activate_users
    :noindex:

    Whether newly registered accounts are automatically activated or require manual
    activation by a sysadmin.

    Defaults to ``True``.

.. confval:: email_confirmation_required
    :noindex:

    Whether email confirmation is required before any features of |kadi| can be used.

    Defaults to ``False``.

.. confval:: allow_registration
    :noindex:

    Whether to allow users to register their own local accounts via the GUI of |kadi|.

    Defaults to ``False``.

    .. tip::
        :ref:`Sysadmins <installation-configuration-sysadmins>` can also create local
        accounts manually via the graphical sysadmin interface. The same is possible via
        the Kadi CLI after installing and configuring |kadi| successfully:

        .. code-block:: bash

            sudo su - kadi    # Switch to the kadi user (in production environments)
            kadi users create # Create a new local user (either interactively or by providing suitable parameters)

LDAP
~~~~

Accounts based on LDAP (Lightweight Directory Access Protocol) authentication are
managed by a directory service implementing the LDAP protocol. The most important user
attributes are handled the following:

* **Username:** Retrieved via LDAP and cannot be changed.
* **Email:** Retrieved via LDAP. Cannot be changed, but is confirmed automatically. If
  the email stored in LDAP is updated, it will also be updated in |kadi| the next time
  the user logs in.
* **Password:** Checked against LDAP, but not stored in |kadi|. If configured, users may
  be able to change their LDAP password via |kadi|. Otherwise, changing the password is
  only possible via LDAP directly.

The following configuration options are supported by this provider:

.. confval:: type
    :noindex:

    The type of the authentication provider. Must correspond to ``"ldap"``.

.. confval:: title
    :noindex:

    The title shown on the login page corresponding to the authentication provider.

    Defaults to ``"Login with LDAP"`` (localized).

.. confval:: default_system_role
    :noindex:

    The default system role of newly registered accounts. One of ``"admin"``,
    ``"member"`` or ``"guest"``. Please see the user management section in the graphical
    sysadmin interface for more information about system roles.

    Defaults to ``"member"``.

.. confval:: activate_users
    :noindex:

    Whether newly registered accounts are automatically activated or require manual
    activation by a sysadmin.

    Defaults to ``True``.

.. confval:: active_directory
    :noindex:

    Whether the LDAP server is an Active Directory.

    Defaults to ``False``.

.. confval:: host
    :noindex:

    The IP or hostname of the LDAP server.

    Defaults to ``""``.

.. confval:: port
    :noindex:

    The port of the LDAP server to connect with. The default port is usually ``389``,
    while port ``636`` is generally used together with SSL/TLS.

    Defaults to ``389``.

.. confval:: encryption
    :noindex:

    The encryption method to use. Can be set to ``"ldaps"`` to use SSL/TLS for the
    entire connection or ``"starttls"`` to use STARTTLS.

    Defaults to ``None``.

    .. note::
        If secure connections to the LDAP server fail, this may be a result of newer
        Python versions by default not enabling older SSL/TLS ciphers anymore that offer
        less security (e.g. no forward secrecy). To see which cipher is used by the
        server by default, the following command may be used to test the connection:

        .. code-block:: bash

            openssl s_client -connect <host>:<port>

        At the end of the command's output, details about the SSL/TLS session should be
        listed, including the corresponding SSL/TLS cipher that was used. This value may
        then be configured via the :confval:`ciphers` configuration option.

.. confval:: validate_cert
    :noindex:

    Whether to validate the server's SSL/TLS certificate if an encryption method is set.

    Defaults to ``True``.

.. confval:: ciphers
    :noindex:

    One or more SSL/TLS ciphers to use as a single string according to the OpenSSL
    cipher list format (which separates multiple ciphers with colon) if an encryption
    method is set. May also be set to ``"DEFAULT"``, in which case the default ciphers
    of the installed OpenSSL version are used. Please be aware of the possible security
    implications when changing this value.

    Defaults to ``None``.

.. confval:: users_dn
    :noindex:

    The base DN where users are stored in the LDAP directory, which will be used to
    perform a simple bind with a user for authentication and to retrieve the additional
    user attributes after a successful bind. The former works differently depending on
    whether the server is an Active Directory. For Active Directories, a
    *UserPrincipalName* is constructed from the DN's domain components in the form of
    ``<username>@<domain>``, where ``<username>`` is the specified username when logging
    in. Otherwise, the full bind DN is constructed as
    ``<username_attr>=<username>,<users_dn>``.

    Defaults to ``""``.

.. confval:: bind_user
    :noindex:

    The full DN of a user that should be used to perform any LDAP operations after a
    successful bind. By default, the bound (i.e. authenticated) user will be used for
    these operations.

    Defaults to ``None``.

.. confval:: bind_pw
    :noindex:

    The password of the configured :confval:`bind_user`.

    Defaults to ``None``.

.. confval:: username_attr
    :noindex:

    The LDAP attribute name to use for the (unique) username of the user, e.g. ``"uid"``
    or ``"sAMAccountName"``. Will also be used for the display name as fallback.

    Defaults to ``"uid"``.

.. confval:: email_attr
    :noindex:

    The LDAP attribute name to use for the email of the user.

    Defaults to ``"mail"``.

.. confval:: displayname_attr
    :noindex:

    The LDAP attribute name to use for the display name of the user. If
    :confval:`firstname_attr` and :confval:`lastname_attr` have been specified, these
    two attributes will take precedence.

    Defaults to ``"displayName"``.

.. confval:: firstname_attr
    :noindex:

    The LDAP attribute name to use for the first name of the display name instead of
    :confval:`displayname_attr`, e.g. ``"givenName"``. Must be used together with
    :confval:`lastname_attr`.

    Defaults to ``None``.

.. confval:: lastname_attr
    :noindex:

    The LDAP attribute name to use for the last name of the display name instead of
    :confval:`displayname_attr`, e.g. ``"sn"``. Must be used together with
    :confval:`firstname_attr`.

    Defaults to ``None``.

.. confval:: allow_password_change
    :noindex:

    Whether to allow LDAP users to change their password via Kadi4Mat. This uses LDAP's
    *Password Modify Extended Operation* to perform the password change, so keep in mind
    that any password hashing is done server-side only, i.e. the connection should be
    encrypted.

    Defaults to ``False``.

.. confval:: send_old_password
    :noindex:

    Whether to send the old password when changing it, which might be required for some
    LDAP servers.

    Defaults to ``False``.

OpenID Connect
~~~~~~~~~~~~~~

Accounts based on OpenID Connect (OIDC) authentication are managed by one or more
external web-based services, such as `ORCID <https://orcid.org>`__ or an installation of
`Keycloak <https://www.keycloak.org>`__.  The most important user attributes are handled
the following:

* **Username:** Retrieved via the respective OIDC provider, if possible. Can be changed
  by the user on initial account creation, but not afterwards.
* **Email:** Retrieved via the respective OIDC provider, if possible. Can be changed by
  the user on initial account creation and also afterwards. Usually not confirmed
  automatically, unless an OIDC provider supplies the email, marks it as confirmed and
  it is not changed by the user during initial account creation.
* **Password:** Checked against the respective OIDC provider, but not stored in |kadi|.
  Changing the password is only possible via the respective OIDC provider directly.

The following configuration options are supported by this provider:

.. confval:: type

    The type of the authentication provider. Must correspond to ``"oidc"``.

.. confval:: title
    :noindex:

    The title shown on the login page corresponding to the authentication provider.

    Defaults to ``"Login with OpenID Connect"`` (localized).

.. confval:: default_system_role
    :noindex:

    The default system role of newly registered accounts. One of ``"admin"``,
    ``"member"`` or ``"guest"``. Please see the user management section in the graphical
    sysadmin interface for more information about system roles.

    Defaults to ``"member"``.

.. confval:: activate_users
    :noindex:

    Whether newly registered accounts are automatically activated or require manual
    activation by a sysadmin.

    Defaults to ``True``.

.. confval:: email_confirmation_required
    :noindex:

    Whether email confirmation is required before any features of |kadi| can be used.

    Defaults to ``False``.

.. confval:: providers
    :noindex:

    A list of dictionaries, each dictionary specifying a supported OIDC provider with
    its specific configuration in a form similar to the authentication provider
    configuration itself.

    Defaults to ``[]``.

    .. confval:: name
        :noindex:

        A name to uniquely identify the provider. Mainly used internally.

        Defaults to ``""``.

    .. confval:: title
        :noindex:

        An optional title of the provider, which will be used in a corresponding login
        button on the login page.

        Defaults to the :confval:`name` of the provider.

    .. confval:: icon
        :noindex:

        An optional icon of the provider, which will be used in a corresponding login
        button on the login page next to the :confval:`title`. Currently, only (free)
        icons provided by the `Font Awesome <https://fontawesome.com>`__ icon library
        are supported. Icons have to be specified using the corresponding CSS classes,
        e.g. ``"fa-brands fa-orcid"``.

        Defaults to ``None``.

    .. confval:: client_id
        :noindex:

        The OIDC client ID obtained from the provider.

        In order to obtain a client ID (and client secret), a |kadi| instance has to be
        registered as an OIDC application first. The specific name of such an
        integration as well as the configuration values that need to be provided depend
        on the respective provider. The most important settings usually include:

        * **Redirect URI:** Specifying a URI that a provider can redirect to after users
          authenticate is a required part of the OIDC/OAuth2 flow. For |kadi|, this URI
          needs to be of the form
          ``https://<server_name>/oidc/authorize/<provider_name>``, where
          ``<server_name>`` corresponds to the configured :confval:`SERVER_NAME` of
          |kadi|, while the ``<provider_name>`` corresponds to the configured
          :confval:`name` of the respective provider.
        * **Client type:** Some providers distinguish between different types of
          clients, depending on whether they can keep a client secret safe. As this is
          the case for |kadi|, it should be added as a *confidential* application, if
          applicable, which usually corresponds to the *Authorization Code* grant type
          in the context of OAuth2.
        * **Scopes:** Scopes can determine the amount of user information that |kadi|
          will receive from the provider. |kadi| automatically requests the standard
          OIDC scopes ``openid``, ``profile`` and ``email``. Some providers require
          specifying these scopes during registration, in which case the same three
          scopes should be selected.

        Defaults to ``""``.

    .. confval:: client_secret
        :noindex:

        The OIDC client secret obtained from the provider. See the :confval:`client_id`
        configuration option on information on how to obtain it.

        Defaults to ``""``.

    .. confval:: discovery_url
        :noindex:

        The OIDC discovery URL of the provider to automatically configure the
        authentication flow, usually in the form of
        ``https://<base_url>/.well-known/openid-configuration``, where the
        ``<base_url>`` is the base URL of the respective provider.

        Defaults to ``""``.

.. _installation-configuration-auth-shibboleth:

Shibboleth
~~~~~~~~~~

Accounts based on Shibboleth authentication are managed by one or more external
authentication providers. Conceptually, Shibboleth consists of a Service Provider (SP),
in this case |kadi|, and one or multiple Identity Providers (IdPs), which maintain and
authenticate the user accounts. An IdP may either be a single organization or a
federation of multiple organizations, such as the `DFN-AAI <https://doku.tid.dfn.de>`__
or the `SWITCHaai <https://www.switch.ch/aai>`__, to facilitate the integration of
multiple IdPs.

The most important user attributes are handled the following:

* **Username:** Retrieved via the respective IdP and cannot be changed.
* **Email:** Retrieved via the respective IdP. Cannot be changed, but is confirmed
  automatically. If the email supplied by the IdP is updated, it will also be updated in
  |kadi| the next time the user logs in.
* **Password:** Checked against the respective IdP, but not stored in |kadi|. Changing
  the password is only possible via the respective organization directly.

.. note::
    As setting up Shibboleth requires additional dependencies and configuration in
    addition to the |kadi| configuration explained above, please also refer to the setup
    instructions explained in the :ref:`next section
    <installation-configuration-shib-setup>`.

The following configuration options are supported by this provider:

.. confval:: type
    :noindex:

    The type of the authentication provider. Must correspond to ``"shib"``.

.. confval:: title
    :noindex:

    The title shown on the login page corresponding to the authentication provider.

    Defaults to ``"Login with Shibboleth"`` (localized).

.. confval:: default_system_role
    :noindex:

    The default system role of newly registered accounts. One of ``"admin"``,
    ``"member"`` or ``"guest"``. Please see the user management section in the graphical
    sysadmin interface for more information about system roles.

    Defaults to ``"member"``.

.. confval:: activate_users
    :noindex:

    Whether newly registered accounts are automatically activated or require manual
    activation by a sysadmin.

    Defaults to ``True``.

.. confval:: env_encoding
    :noindex:

    The encoding of the environment variables set via the Apache Shibboleth module
    containing the user and meta attributes.

    Defaults to ``"latin-1"``.

.. confval:: multivalue_separator
    :noindex:

    The separator to use for splitting attributes containing multiple values.

    Defaults to ``";"``.

.. confval:: sp_entity_id
    :noindex:

    The entityID of the SP.

    Defaults to ``"https://<server_name>/shibboleth"``, where ``<server_name>``
    corresponds to the configured :confval:`SERVER_NAME` of |kadi|.

.. confval:: sp_session_initiator
    :noindex:

    The path of the session (login) initiator of the SP.

    Defaults to ``"/Shibboleth.sso/Login"``.

.. confval:: sp_logout_initiator
    :noindex:

    The path of the (local) logout initiator of the SP.

    Defaults to ``"/Shibboleth.sso/Logout"``.

.. confval:: idp_entity_id_attr
    :noindex:

    The Shibboleth attribute name to extract the entityID of the current IdP from.

    Defaults to ``"Shib-Identity-Provider"``.

.. confval:: idp_displayname_attr
    :noindex:

    The Shibboleth meta attribute name to extract the display name of the current IdP
    from.

    Defaults to ``"Meta-displayName"``.

.. confval:: idp_support_contact_attr
    :noindex:

    The Shibboleth meta attribute name to extract the support email address of the
    current IdP from.

    Defaults to ``"Meta-supportContact"``.

.. confval:: username_attr
    :noindex:

    The Shibboleth attribute name to use for the (unique) username of the user, which
    should correspond to the attribute used to extract the *eduPersonPrincipalName*.
    This name specifies a (generally) human-friendly username, which is combined with an
    IdP-specific scope value in the form of ``<username>@<scope>``, making it globally
    unique. Will also be used for the display name as fallback.

    Defaults to ``"eppn"``.

.. confval:: email_attr
    :noindex:

    The Shibboleth attribute name to use for the email of the user.

    Defaults to ``"mail"``.

.. confval:: displayname_attr
    :noindex:

    The Shibboleth attribute name to use for the display name of the user.

    Defaults to ``"displayName"``.

.. confval:: firstname_attr
    :noindex:

    The Shibboleth attribute name to use for the first name. Only used if no display
    name could be found, in combination with :confval:`lastname_attr`.

    Defaults to ``"givenName"``.

.. confval:: lastname_attr
    :noindex:

    The Shibboleth attribute name to use for the last name. Only used if no display name
    could be found, in combination with :confval:`firstname_attr`.

    Defaults to ``"sn"``.

.. confval:: idps
    :noindex:

    A list of dictionaries, each dictionary specifying a supported Shibboleth IdP with
    its specific configuration in a form similar to the authentication provider
    configuration itself.

    Defaults to ``[]``.

    .. confval:: name
        :noindex:

        The name of the IdP, which will be used for displaying it in a corresponding
        selection.

        Defaults to ``""``.

    .. confval:: entity_id
        :noindex:

        The entityID of the IdP.

        Defaults to ``""``.

.. _installation-configuration-shib-setup:

Setting up Shibboleth
---------------------

Shibboleth has currently been tested with the official Shibboleth **Service Provider 3**
in combination with the Apache web server using the **mod_shib** module, which the
following instructions are also based on. As configuring Shibboleth correctly heavily
depends on the specific IdP(s) that should be used, the following sections attempt to
provide the most important general and |kadi|-specific instructions to make Shibboleth
authentication work.

Obtaining a certificate
~~~~~~~~~~~~~~~~~~~~~~~

The SP requires a certificate in order to sign authentication requests and to decrypt
SAML assertions. Usually, this certificate may be self-signed and differs from the
server certificate that |kadi| already uses for HTTPS. The requirements for this
certificate and how long it may be valid differs per IdP. A good starting point to
generate a certificate is the documentation of the `SWITCHaai
<https://www.switch.ch/aai/guides/sp/configuration/#4>`__.

Once generated, the certificate and key files should be moved to a suitable place on the
machine where |kadi| is installed and given suitable permissions:

.. code-block:: bash

    sudo mv <cert_file> /etc/ssl/certs
    sudo chmod 644 /etc/ssl/certs/<cert_file>
    sudo chown root:root /etc/ssl/certs/<cert_file>

    sudo mv <key_file> /etc/ssl/private
    sudo chmod 640 /etc/ssl/private/<key_file>
    sudo chown root:ssl-cert /etc/ssl/private/<key_file>

.. note::
    Independent of how the certificate is obtained, once it is expired, a `certificate
    rollover <https://www.switch.ch/aai/guides/sp/certificate-rollover>`__ has to be
    performed.

Installing additional dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Shibboleth SP and the corresponding Apache module have to be installed and enabled
by running:

.. code-block:: bash

    sudo apt install libapache2-mod-shib
    sudo a2enmod shib

During the installation, a new user *_shibd* will be created automatically. This user
needs access to the certificate used by Shibboleth later on, so it should be added to
the *ssl-cert* group:

.. code-block:: bash

    sudo usermod -a -G ssl-cert _shibd

Configuring Shibboleth
~~~~~~~~~~~~~~~~~~~~~~

In order to configure Shibboleth, the main configuration file at
``/etc/shibboleth/shibboleth2.xml`` needs to be adjusted. Before doing so, it might be a
good idea to create a backup of it. Generally, some of this configuration may depend on
the specific IdPs that should be configured, however, the following sections list the
most important XML elements that require changes:

.. confval:: ApplicationDefaults
    :noindex:

    This element configures the general behavior of the SP. The following two attributes
    need to be specified or changed:

    .. code-block:: xml

        <ApplicationDefaults entityID="<sp_entity_id>"
                             metadataAttributePrefix="Meta-">
                             <!-- Other attributes. -->

            <!-- Other tags. -->
        </ApplicationDefaults>

    * **entityID**: The unique identifier of the SP, which should match the value
      configured via the :confval:`sp_entity_id` authentication provider setting in
      |kadi|.
    * **metadataAttributePrefix**: If set to ``Meta-``, |kadi| can extract various
      Shibboleth meta attributes without changing the default configuration of the
      authentication provider in |kadi|.

.. confval:: Sessions
    :noindex:

    This element configures how SSO sessions should be handled by the SP. The following
    attribute needs to be specified or changed:

    .. code-block:: xml

        <Sessions redirectLimit="exact">
                  <!-- Other attributes. -->

            <!-- Other tags. -->
        </Sessions>

    * **redirectLimit** While not required by |kadi|, it is highly recommended to change
      the value of the ``redirectLimit`` attribute to ``exact``, if not already the
      case, as the SP might be left vulnerable to open redirect attacks otherwise.

.. confval:: SSO
    :noindex:

    This element specifies the configured single sign-on procotols within the SP and
    generally depends on the IdPs that should be used. However, as |kadi| already
    provides a selection ("discovery") of IdPs on its login page, configured via the
    :confval:`idps` authentication provider setting in |kadi|, no attributes should be
    necessary here and any default attributes can be removed. The content of the element
    should be set to ``SAML2``.

    .. code-block:: xml

        <SSO>SAML2</SSO>

.. confval:: Logout
    :noindex:

    This element specifies the configured logout protocols for the SP. The easiest
    method is local logout, which will only be performed for the current SP, without any
    IdP involvement. To enable it, the content of the element should be set to only
    ``Local``. No attributes should be necessary.

    .. code-block:: xml

        <Logout>Local</Logout>

.. confval:: MetadataProvider
    :noindex:

    This element specifies how metadata are exchanged between the SP and the IdPs and
    therefore heavily depends on the IdPs that should be used. As metadata are usually
    distributed using XML files, a basic example of specifying a metadata provider,
    which may contain metadata for one or multiple IdPs, may look like the following:

    .. code-block:: xml

        <MetadataProvider type="XML"
                          url="https://idp.example.com/idp-medatata.xml"
                          backingFilePath="idp-medatata.xml"
                          reloadInterval="3600"/>
        </MetadataProvider>

.. confval:: AttributeExtractor
    :noindex:

    This element specifies how different kinds of attributes will be extracted from
    different sources. In order to extract additional IdP metadata, which |kadi| can use
    for displaying errors about missing (required) attributes to users, the following
    extractor should be defined, in addition to any default extractors:

    .. code-block:: xml

        <AttributeExtractor type="Metadata"
                            DisplayName="displayName">
            <ContactPerson id="supportContact"
                           contactType="support"
                           formatter="$EmailAddress"/>
        </AttributeExtractor>

.. confval:: CredentialResolver
    :noindex:

    This element specifies the certificate and key files to use for signing and
    decrypting messages and assertions. Here, the corresponding files obtained in the
    first step have to be configured:

    .. code-block:: xml

        <CredentialResolver type="Chaining">
            <!-- Active certificate. -->
            <CredentialResolver type="File"
                                key="/etc/ssl/private/<key_file>"
                                certificate="/etc/ssl/certs/<cert_file>"/>

            <!-- Additional certificates, e.g. when performing a certificate rollover. -->
        </CredentialResolver>

Once Shibboleth itself is configured, the attribute map file at
``/etc/shibboleth/attribute-map.xml`` needs to be adjusted as well. This file specifies
the attributes that Shibboleth should attempt to extract based on the information
retrieved from an IdP. Usually, the default map should mostly work already, however, the
following LDAP-based attributes might need to be enabled (or added, if not already
present) in addition:

.. code-block:: xml

    <Attribute name="urn:oid:2.5.4.3" id="cn"/>
    <Attribute name="urn:oid:2.5.4.4" id="sn"/>
    <Attribute name="urn:oid:2.5.4.42" id="givenName"/>
    <Attribute name="urn:oid:2.16.840.1.113730.3.1.241" id="displayName"/>
    <Attribute name="urn:oid:0.9.2342.19200300.100.1.3" id="mail"/>

Finally, the Shibboleth daemon has to be restarted to pick up all changes:

.. code-block:: bash

    sudo systemctl restart shibd

Afterwards, it is recommended to take a look at the corresponding log file at
``/var/log/shibboleth/shibd.log`` to identify any potential configuration errors.

Configuring Apache
~~~~~~~~~~~~~~~~~~

In order to configure Shibboleth in the Apache web server, the corresponding
configuration file has to be adjusted, e.g. ``/etc/apache2/sites-available/kadi.conf``
when using the structure as described in the installation instructions:

.. code-block:: apache

    # Before "ProxyPass / uwsgi:..."
    ProxyPass /Shibboleth.sso !

    # After "<Location /static>...</Location>"
    <Location />
        AuthType shibboleth
        Require shibboleth
    </Location>

This configuration ensures that all requests to ``/Shibboleth.sso/*`` will be handled by
Shibboleth (assuming this prefix was not changed via any other Shibboleth or |kadi|
setting), while the directive below defines a global rule to enable Shibboleth on all
pages. This way, Shibboleth will generally be active, but won't block access to any
pages, as this will be handled by |kadi| directly.

Configuring Kadi4Mat
~~~~~~~~~~~~~~~~~~~~

Once all external dependencies are installed and configured, all that is left is to
configure |kadi| :ref:`itself <installation-configuration-auth-shibboleth>`.

.. note::
    Even if configured successfully, authentication might not work out-of-the-box, as
    individual SPs might need to be enabled manually in each IdP that should be used by
    the corresponding IdP administrators. Some IdPs might also not release all required
    attributes to |kadi| by default. In this case, users will be presented with a
    corresponding error page when attempting to log in to |kadi|.
