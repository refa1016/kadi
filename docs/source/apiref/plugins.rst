.. _apiref-plugins:

Plugin specs
============

This section contains an API reference of the :mod:`kadi.plugins.spec` Python module,
which contains the hook specifications that currently exist for use in plugins. For
implementing any of the plugin hooks, see also how to :ref:`develop plugins
<development-plugins>`.

.. automodule:: kadi.plugins.spec
    :members:
