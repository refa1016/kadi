# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import current_app
from limits import parse_many

from kadi.ext.limiter import limiter
from kadi.lib.utils import named_tuple
from kadi.lib.web import url_for
from tests.utils import check_api_response
from tests.utils import check_view_response


def _exhaust_rate_limit(
    client, endpoint, limit_per_sec, is_api_response=False, is_limited=True
):
    # Reset the limit each time, as we are only testing global limits.
    limiter.reset()

    for i in range(limit_per_sec + 1):
        response = client.get(url_for(endpoint))

        if i < limit_per_sec or not is_limited:
            assert response.status_code != 429
        else:
            if is_api_response:
                check_api_response(response, status_code=429)
                assert "Rate limit exceeded" in response.get_json()["description"]
            else:
                check_view_response(response, status_code=429)
                assert "Rate limit exceeded" in response.get_data(as_text=True)

            assert "X-RateLimit-Limit" in response.headers
            assert "X-RateLimit-Remaining" in response.headers
            assert "X-RateLimit-Reset" in response.headers

        # Make sure that static endpoints are always excluded.
        response = client.get(url_for("static", filename="favicons/favicon.ico"))
        response.close()

        assert response.status_code != 429


def test_ip_whitelist(monkeypatch, client):
    """Test the IP whitelist for rate limiting."""
    limit_per_sec = 0

    for limit in parse_many(current_app.config["RATELIMIT_ANONYMOUS_USER"]):
        if limit.GRANULARITY.name == "second":
            limit_per_sec = limit.amount

    _exhaust_rate_limit(client, "main.index", limit_per_sec, is_limited=False)
    _exhaust_rate_limit(
        client, "api.index", limit_per_sec, is_api_response=True, is_limited=False
    )

    monkeypatch.setitem(current_app.config, "RATELIMIT_IP_WHITELIST", [])

    _exhaust_rate_limit(client, "main.index", limit_per_sec)
    _exhaust_rate_limit(client, "api.index", limit_per_sec, is_api_response=True)


def test_app_limits_anonymous(monkeypatch, client):
    """Test the global rate limit per second for anonymous users."""
    monkeypatch.setitem(current_app.config, "RATELIMIT_IP_WHITELIST", [])

    for limit in parse_many(current_app.config["RATELIMIT_ANONYMOUS_USER"]):
        if limit.GRANULARITY.name == "second":
            _exhaust_rate_limit(client, "main.index", limit.amount)
            _exhaust_rate_limit(client, "api.index", limit.amount, is_api_response=True)


def test_app_limits_authenticated(monkeypatch, client):
    """Test the global rate limit per second for authenticated users."""
    monkeypatch.setitem(current_app.config, "RATELIMIT_IP_WHITELIST", [])

    # Using the normal user fixtures seems to cause issues with the test client and the
    # way Flask-Limiter works internally.
    monkeypatch.setattr(
        "kadi.ext.limiter.current_user", named_tuple("User", is_authenticated=True)
    )

    for limit in parse_many(current_app.config["RATELIMIT_AUTHENTICATED_USER"]):
        if limit.GRANULARITY.name == "second":
            _exhaust_rate_limit(client, "main.index", limit.amount)
            _exhaust_rate_limit(client, "api.index", limit.amount, is_api_response=True)
