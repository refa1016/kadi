# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import current_app
from werkzeug.exceptions import default_exceptions
from werkzeug.http import HTTP_STATUS_CODES

from kadi.lib.api.core import check_access_token_scopes
from kadi.lib.api.core import get_access_token
from kadi.lib.api.core import json_error_response
from kadi.lib.api.core import json_response
from kadi.lib.api.models import PersonalToken
from kadi.lib.oauth.models import OAuth2ServerToken
from kadi.lib.web import url_for
from tests.utils import check_api_response


def test_json_response():
    """Test if JSON responses work correctly."""
    response = json_response(200, {"test": "test"})

    check_api_response(response)
    assert "test" in response.get_json()


def test_json_error_response():
    """Test if JSON error responses work correctly."""
    response = json_error_response(
        404, message="message", description="description", test="test"
    )
    data = response.get_json()

    check_api_response(response, status_code=404)
    assert data["code"] == 404
    assert data["message"] == "message"
    assert data["description"] == "description"
    assert data["test"] == "test"

    # Check the default content.
    response = json_error_response(404)
    data = response.get_json()

    check_api_response(response, status_code=404)
    assert data["code"] == 404
    assert data["message"] == HTTP_STATUS_CODES.get(404)
    assert data["description"] == default_exceptions.get(404).description


@pytest.mark.parametrize("token_type", ["pat", "oat"])
@pytest.mark.parametrize("include_prefix", [True, False])
def test_get_access_token(
    token_type,
    include_prefix,
    api_client,
    client,
    new_oauth2_server_token,
    new_personal_token,
):
    """Test if access tokens are retrieved correctly."""
    assert get_access_token() is None

    with client:
        client.get(url_for("api.index"))
        assert get_access_token() is None

    if token_type == "pat":
        token = PersonalToken.new_token(include_prefix=include_prefix)
        new_personal_token(token=token)
    else:
        token = OAuth2ServerToken.new_access_token(include_prefix=include_prefix)
        new_oauth2_server_token(access_token=token)

    with api_client(token) as _api_client:
        _api_client.get(url_for("api.index"))
        access_token = get_access_token()

        if token_type == "pat":
            assert access_token is not None
            assert access_token.token_hash == PersonalToken.hash_token(token)
        else:
            if include_prefix:
                assert access_token is not None
                assert access_token.access_token == OAuth2ServerToken.hash_token(token)
            else:
                # OAuth2 tokens won't work without a prefix, as only personal tokens are
                # taken as fallback.
                assert access_token is None


def test_check_access_token_scopes(new_personal_token):
    """Test if scopes of access tokens are checked correctly."""

    # Test without a request context.
    assert check_access_token_scopes("test.test")

    # Test an access token with full access.
    token = new_personal_token()

    with current_app.test_request_context(headers={"Authorization": f"Bearer {token}"}):
        assert check_access_token_scopes("test.test")

    # Test a scoped access token.
    token = new_personal_token(scope="collection.read record.read")

    with current_app.test_request_context(headers={"Authorization": f"Bearer {token}"}):
        assert check_access_token_scopes("collection.read")
        assert check_access_token_scopes("record.read")
        assert check_access_token_scopes(
            "record.read", "collection.read", operator="and"
        )
        assert check_access_token_scopes(
            "record.read", "collection.read", "group.read", operator="or"
        )

        assert not check_access_token_scopes("template.read")
        assert not check_access_token_scopes("test.test")
        assert not check_access_token_scopes(
            "template.read", "group.read", operator="or"
        )
        assert not check_access_token_scopes(
            "record.read", "collection.read", "template.read", operator="and"
        )


def test_scopes_required(
    api_client, client, dummy_record, new_personal_token, user_session
):
    """Test if the "scopes_required" decorator works correctly."""
    endpoint = url_for("api.get_record", id=dummy_record.id)

    # Test the session without any access token.
    with user_session():
        response = client.get(endpoint)
        check_api_response(response)

    # Test an access token with full access.
    token = new_personal_token()
    response = api_client(token).get(endpoint)
    check_api_response(response)

    # Test a scoped access token without access.
    token = new_personal_token(scope="collection.read")
    response = api_client(token).get(endpoint)
    data = response.get_json()

    check_api_response(response, status_code=401)
    assert data["description"] == "Access token has insufficient scope."
    assert data["scopes"] == ["record.read"]

    # Test a scoped access token with access.
    token = new_personal_token(scope="record.read")
    response = api_client(token).get(endpoint)
    check_api_response(response)


def test_internal(api_client, client, dummy_personal_token, user_session):
    """Test if the "internal" decorator works correctly."""
    endpoint = url_for("api.select_tags")

    with user_session():
        response = client.get(endpoint)
        check_api_response(response)

    response = api_client(dummy_personal_token).get(endpoint)
    check_api_response(response, status_code=404)
