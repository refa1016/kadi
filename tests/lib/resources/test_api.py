# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.permissions.core import add_role as add_role_core
from kadi.lib.permissions.models import Role
from kadi.lib.resources.api import add_link
from kadi.lib.resources.api import add_role as add_role_api
from kadi.lib.resources.api import change_role
from kadi.lib.resources.api import get_resource_group_roles
from kadi.lib.resources.api import get_resource_user_roles
from kadi.lib.resources.api import remove_link
from kadi.lib.resources.api import remove_role
from kadi.lib.resources.api import toggle_favorite_resource
from tests.utils import check_api_response


def test_add_link(dummy_collection, dummy_record, dummy_user, new_user):
    """Test if adding links via the API works correctly."""
    response = add_link(dummy_record.collections, dummy_collection, user=new_user())

    check_api_response(response, status_code=403)
    assert not dummy_record.collections.all()

    response = add_link(dummy_record.collections, dummy_collection, user=dummy_user)

    check_api_response(response, status_code=201)
    assert dummy_record.collections.one() == dummy_collection

    response = add_link(dummy_record.collections, dummy_collection, user=dummy_user)

    check_api_response(response, status_code=409)
    assert dummy_record.collections.one() == dummy_collection


def test_remove_link(dummy_collection, dummy_record, dummy_user, new_user):
    """Test if removing links via the API works correctly."""
    response = remove_link(dummy_record.collections, dummy_collection, user=new_user())

    check_api_response(response, status_code=403)
    assert not dummy_record.collections.all()

    response = remove_link(dummy_record.collections, dummy_collection, user=dummy_user)

    check_api_response(response, status_code=404)
    assert not dummy_record.collections.all()

    add_link(dummy_record.collections, dummy_collection, user=dummy_user)
    response = remove_link(dummy_record.collections, dummy_collection, user=dummy_user)

    check_api_response(response, status_code=204)
    assert not dummy_record.collections.all()


def test_add_role(dummy_record, dummy_user, new_group, new_user):
    """Test if adding roles via the API works correctly."""
    user = new_user()
    object_name = "record"

    response = add_role_api(user, dummy_record, "test", user=dummy_user)

    check_api_response(response, status_code=400)
    assert not user.roles.filter(Role.object == object_name).all()

    response = add_role_api(user, dummy_record, "member", user=dummy_user)

    check_api_response(response, status_code=201)
    assert user.roles.filter(Role.object == object_name).one().name == "member"

    response = add_role_api(user, dummy_record, "member", user=dummy_user)

    check_api_response(response, status_code=409)
    assert user.roles.filter(Role.object == object_name).one().name == "member"

    # Try adding a role to a non-readable group.
    group = new_group(creator=user)
    response = add_role_api(group, dummy_record, "member", user=dummy_user)

    check_api_response(response, status_code=403)
    assert not group.roles.filter(Role.object == object_name).all()


def test_remove_role(dummy_record, dummy_user, new_user):
    """Test if removing roles via the API works correctly."""
    user = new_user()
    object_name = "record"

    response = remove_role(dummy_user, dummy_record)

    check_api_response(response, status_code=409)
    assert dummy_user.roles.filter(Role.object == object_name).one().name == "admin"

    response = remove_role(user, dummy_record)

    check_api_response(response, status_code=404)
    assert not user.roles.filter(Role.object == object_name).all()

    add_role_core(user, "record", dummy_record.id, "member")
    response = remove_role(user, dummy_record)

    check_api_response(response, status_code=204)
    assert not user.roles.filter(Role.object == object_name).all()


def test_change_role(db, dummy_record, dummy_user, new_user):
    """Test if changing roles via the API works correctly."""
    user = new_user()
    object_name = "record"

    response = change_role(dummy_user, dummy_record, "member")

    check_api_response(response, status_code=409)
    assert "Cannot change the creator's role." in response.get_json()["description"]
    assert dummy_user.roles.filter(Role.object == object_name).one().name == "admin"

    response = change_role(user, dummy_record, "member")

    check_api_response(response, status_code=404)
    assert not user.roles.filter(Role.object == object_name).all()

    add_role_core(user, "record", dummy_record.id, "member")
    response = change_role(user, dummy_record, "editor")

    check_api_response(response, status_code=204)
    assert user.roles.filter(Role.object == object_name).one().name == "editor"

    # Start a new savepoint, so only the latest changes (removing the role) are rolled
    # back.
    db.session.begin_nested()
    response = change_role(user, dummy_record, "test")
    db.session.rollback()

    check_api_response(response, status_code=400)
    assert user.roles.filter(Role.object == object_name).one().name == "editor"


def test_toggle_favorite_resource(dummy_record, dummy_user):
    """Test if toggling the favorite state of a resource works correctly."""
    assert not dummy_user.favorites.all()

    toggle_favorite_resource(resource=dummy_record, user=dummy_user)
    assert dummy_user.favorites.one()

    toggle_favorite_resource(resource=dummy_record, user=dummy_user)
    assert not dummy_user.favorites.all()


def test_get_resource_user_roles(dummy_group, dummy_record, dummy_user, new_user):
    """Test if determining the user roles of a resource works correctly."""

    # Create some additional user roles, one of which will be excluded.
    user = new_user()
    add_role_core(user, "record", dummy_record.id, "member")
    add_role_core(new_user(), "record", dummy_record.id, "member")

    user_roles, total = get_resource_user_roles(dummy_record, exclude=[user.id])

    assert len(user_roles) == total == 2

    for user_role in user_roles:
        if user_role["user"]["id"] == dummy_user.id:
            assert user_role["role"]["name"] == "admin"
        else:
            assert user_role["role"]["name"] == "member"


def test_get_resource_group_roles(
    dummy_group, dummy_record, dummy_user, new_group, new_user
):
    """Test if determining the group roles of a resource works correctly."""

    # Create a new group that is not readable by the dummy user.
    group = new_group(new_user())
    add_role_core(dummy_group, "record", dummy_record.id, "member")
    add_role_core(group, "record", dummy_record.id, "member")

    group_roles, total = get_resource_group_roles(dummy_record, user=dummy_user)

    assert len(group_roles) == total == 2

    for group_role in group_roles:
        assert group_role["role"]["name"] == "member"

        # Check whether the new group only contains some basic attributes, e.g. not the
        # description.
        if group_role["group"]["id"] == group.id:
            assert group_role["group"].get("description") is None
        else:
            assert group_role["group"].get("description") is not None
