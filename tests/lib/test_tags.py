# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.tags.models import Tag


def test_tagging_mixin(dummy_record):
    """Test if the "TaggingMixin" works correctly."""
    assert dummy_record.set_tags(["test"])
    assert Tag.query.count() == 1
    assert dummy_record.tags.count() == 1
    assert dummy_record.tags.one() == Tag.query.one()

    assert dummy_record.set_tags(["test2", "test"])
    assert Tag.query.count() == 2
    assert dummy_record.tags.count() == 2

    assert dummy_record.set_tags(["test2", "test2"])
    assert Tag.query.count() == 2
    assert dummy_record.tags.count() == 1
    assert dummy_record.tags.one() == Tag.query.filter_by(name="test2").one()


def test_tag_get_or_create():
    """Test if getting or creating a tag works correctly."""
    tag1 = Tag.get_or_create(name="test")
    tag2 = Tag.get_or_create(name="test")

    assert tag1 == tag2
